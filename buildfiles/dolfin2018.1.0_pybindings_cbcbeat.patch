diff --git a/python/dolfin/__init__.py b/python/dolfin/__init__.py
index 5e265dd04..1391ec40b 100644
--- a/python/dolfin/__init__.py
+++ b/python/dolfin/__init__.py
@@ -108,7 +108,7 @@ from .cpp.la import (IndexMap, DefaultFactory, Matrix, Vector, Scalar,
                      BlockMatrix, BlockVector)
 from .cpp.la import GenericVector  # Remove when pybind11 transition complete
 from .cpp.log import (info, Table, set_log_level, get_log_level, LogLevel,
-                      Progress)
+                      Progress, begin, end, error, warning)
 from .cpp.math import ipow, near, between
 from .cpp.mesh import (Mesh, MeshTopology, MeshGeometry, MeshEntity,
                        MeshColoring, CellType, Cell, Facet, Face,
@@ -182,7 +182,8 @@ from .mesh.meshfunction import (MeshFunction)
 from .mesh.meshvaluecollection import MeshValueCollection
 from .mesh.subdomain import CompiledSubDomain
 
-from .multistage.multistagescheme import (RK4, CN2, ExplicitMidPoint,
+from .multistage.multistagescheme import (RK4, CN2, CrankNicolson,
+                                          ExplicitMidPoint,
                                           ESDIRK3, ESDIRK4,
                                           ForwardEuler, BackwardEuler)
 from .multistage.multistagesolvers import PointIntegralSolver, RKSolver
diff --git a/python/dolfin/fem/form.py b/python/dolfin/fem/form.py
index e84dc3d6e..911a9f19b 100644
--- a/python/dolfin/fem/form.py
+++ b/python/dolfin/fem/form.py
@@ -37,9 +37,11 @@ class Form(cpp.fem.Form):
         # FIXME: move getting include paths to elsewhere
         if form_compiler_parameters is None:
             form_compiler_parameters = {"external_include_dirs": dolfin_pc["include_dirs"]}
-        else:
-            # FIXME: add paths if dict entry already exists
-            form_compiler_parameters["external_include_dirs"] = dolfin_pc["include_dirs"]
+        # FIXME: This does not work if form_compiler_parameters is dolfin::Parameters, as
+        # dolfin_pc["include_dirs"] is a list.
+        # else:
+        #     # FIXME: add paths if dict entry already exists
+        #     form_compiler_parameters["external_include_dirs"] = dolfin_pc["include_dirs"]
 
         ufc_form = ffc_jit(form, form_compiler_parameters=form_compiler_parameters,
                            mpi_comm=mesh.mpi_comm())
diff --git a/python/dolfin/fem/projection.py b/python/dolfin/fem/projection.py
index ffbeea8af..23ae38676 100644
--- a/python/dolfin/fem/projection.py
+++ b/python/dolfin/fem/projection.py
@@ -109,7 +109,7 @@ def project(v, V=None, bcs=None, mesh=None,
         # Assemble linear system
         A = assemble_multimesh(a, form_compiler_parameters=form_compiler_parameters)
         b = assemble_multimesh(L, form_compiler_parameters=form_compiler_parameters)
-
+        V.lock_inactive_dofs(A, b)
         # Solve linear system for projection
         if function is None:
             function = MultiMeshFunction(V)
diff --git a/python/dolfin/function/expression.py b/python/dolfin/function/expression.py
index bbfb39a17..2f5ef44b5 100644
--- a/python/dolfin/function/expression.py
+++ b/python/dolfin/function/expression.py
@@ -334,6 +334,12 @@ class CompiledExpression(BaseExpression):
             element = _select_element(family=None, cell=cell, degree=degree,
                                       value_shape=value_shape)
 
+        # FIXME: The below is invasive and fragile. Fix multistage so
+        #        this is not required.
+        # Store C++ code and user parameters because they are used by
+        # the the multistage module.
+        self._user_parameters = kwargs
+
         BaseExpression.__init__(self, cell=cell, element=element, domain=domain,
                                 name=name, label=label)
 
diff --git a/python/src/fem.cpp b/python/src/fem.cpp
index a33f4f812..aecb8b0c5 100644
--- a/python/src/fem.cpp
+++ b/python/src/fem.cpp
@@ -314,7 +314,6 @@ namespace dolfin_wrappers
       .def("homogenize", &dolfin::DirichletBC::homogenize)
       .def("method", &dolfin::DirichletBC::method)
       .def("zero", &dolfin::DirichletBC::zero)
-      .def("markers", &dolfin::DirichletBC::markers)
       .def("zero_columns", &dolfin::DirichletBC::zero_columns,
            py::arg("A"), py::arg("b"), py::arg("diagonal_value")=0.0)
       .def("get_boundary_values", [](const dolfin::DirichletBC& instance)
@@ -333,7 +332,7 @@ namespace dolfin_wrappers
            &dolfin::DirichletBC::apply)
       .def("apply", (void (dolfin::DirichletBC::*)(dolfin::GenericMatrix&, dolfin::GenericVector&, const dolfin::GenericVector&) const)
            &dolfin::DirichletBC::apply)
-      .def("user_sub_domain", &dolfin::DirichletBC::user_sub_domain)
+      .def("user_subdomain", &dolfin::DirichletBC::user_sub_domain)
       .def("set_value", &dolfin::DirichletBC::set_value)
       .def("set_value", [](dolfin::DirichletBC& self, py::object value)
            {
@@ -550,7 +549,8 @@ namespace dolfin_wrappers
                std::shared_ptr<dolfin::LinearVariationalSolver>,
                dolfin::Variable>(m, "LinearVariationalSolver")
       .def(py::init<std::shared_ptr<dolfin::LinearVariationalProblem>>())
-      .def("solve", &dolfin::LinearVariationalSolver::solve);
+      .def("solve", &dolfin::LinearVariationalSolver::solve)
+      .def("default_parameters", &dolfin::LinearVariationalSolver::default_parameters);
 
     // dolfin::NonlinearVariationalProblem
     py::class_<dolfin::NonlinearVariationalProblem,
diff --git a/python/src/la.cpp b/python/src/la.cpp
index bd49f3a88..d94ee7c5f 100644
--- a/python/src/la.cpp
+++ b/python/src/la.cpp
@@ -939,6 +939,7 @@ namespace dolfin_wrappers
           { return std::unique_ptr<dolfin::LUSolver>(new dolfin::LUSolver(comm.get(), A, method)); }),
           py::arg("comm"), py::arg("A"), py::arg("method") = "default")
       .def("set_operator", &dolfin::LUSolver::set_operator)
+      .def("default_parameters", &dolfin::LUSolver::default_parameters)
       .def("solve", (std::size_t (dolfin::LUSolver::*)(dolfin::GenericVector&,
                                                        const dolfin::GenericVector&))
            &dolfin::LUSolver::solve)
@@ -993,6 +994,7 @@ namespace dolfin_wrappers
           py::arg("comm"), py::arg("A"), py::arg("method") = "default", py::arg("preconditioner")="default")
       .def("set_operator", &dolfin::KrylovSolver::set_operator)
       .def("set_operators", &dolfin::KrylovSolver::set_operators)
+      .def("default_parameters", &dolfin::KrylovSolver::default_parameters)
       .def("solve", (std::size_t (dolfin::KrylovSolver::*)(dolfin::GenericVector&,
                                                            const dolfin::GenericVector&))
            &dolfin::KrylovSolver::solve)
@@ -1011,6 +1013,7 @@ namespace dolfin_wrappers
       .def(py::init<std::string, std::string>())
       .def(py::init<std::string, std::shared_ptr<dolfin::PETScPreconditioner>>())
       .def(py::init<KSP>())
+      .def("default_parameters", &dolfin::PETScKrylovSolver::default_parameters)
       .def("get_options_prefix", &dolfin::PETScKrylovSolver::get_options_prefix)
       .def("set_options_prefix", &dolfin::PETScKrylovSolver::set_options_prefix)
       .def("get_norm_type", (dolfin::PETScKrylovSolver::norm_type (dolfin::PETScKrylovSolver::*)() const)
diff --git a/python/src/log.cpp b/python/src/log.cpp
index 398fa70ca..151d8a658 100644
--- a/python/src/log.cpp
+++ b/python/src/log.cpp
@@ -62,6 +62,11 @@ namespace dolfin_wrappers
     m.def("set_log_level", &dolfin::set_log_level);
     m.def("get_log_level", &dolfin::get_log_level);
     m.def("log", [](dolfin::LogLevel level, std::string s){ dolfin::log(level, s); });
+    m.def("begin", [](unsigned int debug_level, std::string msg){ dolfin::begin(debug_level, msg);});
+    m.def("begin", [](std::string msg){ dolfin::begin(msg);});
+    m.def("error", [](std::string msg){ dolfin::error(msg);});
+    m.def("warning", [](std::string msg){ dolfin::warning(msg);});
+    m.def("end", &dolfin::end);
 
     // dolfin::Progress
     py::class_<dolfin::Progress, std::shared_ptr<dolfin::Progress>>
diff --git a/python/src/multistage.cpp b/python/src/multistage.cpp
index 6b7eb1619..599f5c240 100644
--- a/python/src/multistage.cpp
+++ b/python/src/multistage.cpp
@@ -66,7 +66,8 @@ namespace dolfin_wrappers
       .def("reset_newton_solver", &dolfin::PointIntegralSolver::reset_newton_solver)
       .def("reset_stage_solutions", &dolfin::PointIntegralSolver::reset_stage_solutions)
       .def("step", &dolfin::PointIntegralSolver::step)
-      .def("step_interval", &dolfin::PointIntegralSolver::step_interval);
+      .def("step_interval", &dolfin::PointIntegralSolver::step_interval)
+      .def("default_parameters", &dolfin::PointIntegralSolver::default_parameters);
 
   }
 }
