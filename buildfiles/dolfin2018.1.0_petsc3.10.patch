diff --git a/dolfin/la/PETScKrylovSolver.cpp b/dolfin/la/PETScKrylovSolver.cpp
index e72909369..82336b598 100644
--- a/dolfin/la/PETScKrylovSolver.cpp
+++ b/dolfin/la/PETScKrylovSolver.cpp
@@ -366,13 +366,25 @@ std::size_t PETScKrylovSolver::solve(PETScVector& x, const PETScVector& b,
     ierr = KSPGetResidualNorm(_ksp, &rnorm);
     if (ierr != 0) petsc_error(ierr, __FILE__, "KSPGetResidualNorm");
     const char *reason_str = KSPConvergedReasons[reason];
+    PCFailedReason pcfailedreason = PC_NOERROR;
+    if (reason == KSP_DIVERGED_PCSETUP_FAILED) {
+      PC pc;
+      ierr = KSPGetPC(_ksp, &pc);
+      if (ierr != 0) petsc_error(ierr, __FILE__, "KSPGetPC");
+      ierr = PCGetSetUpFailedReason(pc, &pcfailedreason);
+      if (ierr != 0) petsc_error(ierr, __FILE__, "PCGetSetUpFailedReason");
+    }
     bool error_on_nonconvergence = this->parameters["error_on_nonconvergence"].is_set() ? this->parameters["error_on_nonconvergence"] : true;
     if (error_on_nonconvergence)
     {
       dolfin_error("PETScKrylovSolver.cpp",
                    "solve linear system using PETSc Krylov solver",
-                   "Solution failed to converge in %i iterations (PETSc reason %s, residual norm ||r|| = %e)",
-                   static_cast<int>(num_iterations), reason_str, rnorm);
+                   "Solution failed to converge in %i iterations (PETSc reason %s%s%s, residual norm ||r|| = %e)",
+                   static_cast<int>(num_iterations), reason_str,
+                   pcfailedreason == PC_NOERROR ? "" : ":",
+                   pcfailedreason == PC_NOERROR ? "" : PCFailedReasons[pcfailedreason],
+                   rnorm);
+
     }
     else
     {
diff --git a/dolfin/la/PETScVector.cpp b/dolfin/la/PETScVector.cpp
index 69e179548..1bfc83fc5 100644
--- a/dolfin/la/PETScVector.cpp
+++ b/dolfin/la/PETScVector.cpp
@@ -749,7 +749,11 @@ void PETScVector::gather(GenericVector& y,
 
   // Perform scatter
   VecScatter scatter;
+#if PETSC_VERSION_GE(3,11,0)
+  ierr = VecScatterCreateWithData(_x, from, _y.vec(), to, &scatter);
+#else
   ierr = VecScatterCreate(_x, from, _y.vec(), to, &scatter);
+#endif
   CHECK_ERROR("VecScatterCreate");
   ierr = VecScatterBegin(scatter, _x, _y.vec(), INSERT_VALUES,
                          SCATTER_FORWARD);
diff --git a/dolfin/nls/PETScSNESSolver.cpp b/dolfin/nls/PETScSNESSolver.cpp
index b3e1d62c5..dc9994932 100644
--- a/dolfin/nls/PETScSNESSolver.cpp
+++ b/dolfin/nls/PETScSNESSolver.cpp
@@ -48,14 +48,23 @@ PETScSNESSolver::_methods
 = { {"default",      {"default SNES method", ""}},
     {"newtonls",     {"Line search method", SNESNEWTONLS}},
     {"newtontr",     {"Trust region method", SNESNEWTONTR}},
+#if PETSC_VERSION_LT(3,10,0)
     {"test",         {"Tool to verify Jacobian approximation", SNESTEST}},
+#endif
     {"ngmres",       {"Nonlinear generalised minimum residual method",
                       SNESNGMRES}},
+    {SNESKSPONLY,    {"Solve the Newton-like linear system once",
+                      SNESKSPONLY}},
+#if PETSC_VERSION_GE(3,11,0)
+    {SNESKSPTRANSPOSEONLY,
+                     {"Solve the transpose of the Newton-like linear system once",
+                      SNESKSPTRANSPOSEONLY}},
+#endif
     {"nrichardson",  {"Richardson nonlinear method (Picard iteration)",
                       SNESNRICHARDSON}},
     {"vinewtonrsls", {"Reduced space active set solver method (for bounds)",
                       SNESVINEWTONRSLS}},
-    {"vinewtonssls", {"Reduced space active set solver method (for bounds)",
+    {"vinewtonssls", {"Semi-smooth Newton solver method (for bounds)",
                       SNESVINEWTONSSLS}},
     {"qn",           {"Limited memory quasi-Newton", SNESQN}},
     {"ncg",          {"Nonlinear conjugate gradient method", SNESNCG}},
